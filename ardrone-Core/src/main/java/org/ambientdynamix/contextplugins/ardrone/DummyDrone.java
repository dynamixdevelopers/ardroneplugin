package org.ambientdynamix.contextplugins.ardrone;

import android.util.Log;
import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.*;
import de.yadrone.base.configuration.ConfigurationManager;
import de.yadrone.base.exception.ARDroneException;
import de.yadrone.base.exception.IExceptionListener;
import de.yadrone.base.navdata.*;
import de.yadrone.base.video.VideoManager;

import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * Created by workshop on 3/17/14.
 */
public class DummyDrone implements IARDrone {

    private final String TAG = this.getClass().getSimpleName();
    private boolean fly;

    public class DummyCommandManager extends CommandManager{

        public DummyCommandManager(InetAddress inetaddr, IExceptionListener excListener) {
            super(null, new IExceptionListener() {
                @Override
                public void exeptionOccurred(ARDroneException exc) {
                    exc.printStackTrace();
                }
            });
        }


        @Override
        public void resetCommunicationWatchDog() {
            
        }

        @Override
        public void setVideoChannel(VideoChannel c) {
            
        }

        @Override
        public CommandManager doFor(long millis) {
            return this;
        }

        @Override
        public CommandManager after(long millis) {
            return this;
        }

        @Override
        public CommandManager waitFor(long millis) {
            return this;
        }

        @Override
        public void schedule(long millis, Runnable runnable) {
            
        }

        @Override
        public CommandManager landing() {
            return this;
        }

        @Override
        public CommandManager flatTrim() {
            return this;
        }

        @Override
        public CommandManager manualTrim(float pitch, float roll, float yaw) {
            return this;
        }

        @Override
        public CommandManager takeOff() {
            return this;
        }

        @Override
        public CommandManager emergency() {
            return this;
        }

        @Override
        public CommandManager forward(int speed) {
            return this;
        }

        @Override
        public CommandManager backward(int speed) {
            return this;
        }

        @Override
        public CommandManager spinRight(int speed) {
            return this;
        }

        @Override
        public CommandManager spinLeft(int speed) {
            return this;
        }

        @Override
        public CommandManager up(int speed) {
            return this;
        }

        @Override
        public CommandManager down(int speed) {
            return this;
        }

        @Override
        public CommandManager goRight(int speed) {
            return this;
        }

        @Override
        public CommandManager goLeft(int speed) {
            return this;
        }

        @Override
        public CommandManager move(float lrtilt, float fbtilt, float vspeed, float aspeed, float magneto_psi, float magneto_psi_accuracy) {
            return this;
        }

        @Override
        public CommandManager move(float lrtilt, float fbtilt, float vspeed, float aspeed) {
            Log.i(TAG, "lrtTilt: " + lrtilt + " fbtilt: " +fbtilt + " vspeed: " + vspeed + " turn: " + aspeed);
            return this;
        }

        @Override
        public CommandManager move(int speedX, int speedY, int speedZ, int speedSpin) {
            return this;
        }

        @Override
        public CommandManager freeze() {
            return this;
        }

        @Override
        public CommandManager hover() {
            return this;
        }

        @Override
        public CommandManager setConfigurationIds() {
            return this;
        }

        @Override
        public CommandManager setVideoCodecFps(int fps) {
            return this;
        }

        @Override
        public CommandManager setVideoBitrateControl(VideoBitRateMode mode) {
            return this;
        }

        @Override
        public CommandManager setVideoBitrate(int rate) {
            return this;
        }

        @Override
        public CommandManager setMaxVideoBitrate(int rate) {
            return this;
        }

        @Override
        public CommandManager setVideoCodec(VideoCodec c) {
            return this;
        }

        @Override
        public CommandManager setVideoOnUsb(boolean b) {
            return this;
        }

        @Override
        public CommandManager setNavDataDemo(boolean b) {
            return this;
        }

        @Override
        public CommandManager setNavDataOptions(int mask) {
            return this;
        }

        @Override
        public CommandManager setLedsAnimation(LEDAnimation anim, float freq, int duration) {
            return this;
        }

        @Override
        public CommandManager setDetectEnemyWithoutShell(boolean b) {
            return this;
        }

        @Override
        public CommandManager setEnemyColors(EnemyColor c) {
            return this;
        }

        @Override
        public CommandManager setDetectionType(CadType type) {
            return this;
        }

        @Override
        public CommandManager setDetectionType(DetectionType dt, VisionTagType[] tagtypes) {
            return this;
        }

        @Override
        public CommandManager setVisionParameters(int coarse_scale, int nb_pair, int loss_per, int nb_tracker_width, int nb_tracker_height, int scale, int trans_max, int max_pair_dist, int noise) {
            return this;
        }

        @Override
        public CommandManager setVisionOption(int option) {
            return this;
        }

        @Override
        public CommandManager setGains(int pq_kp, int r_kp, int r_ki, int ea_kp, int ea_ki, int alt_kp, int alt_ki, int vz_kp, int vz_ki, int hovering_kp, int hovering_ki, int hovering_b_kp, int hovering_b_ki) {
            return this;
        }

        @Override
        public CommandManager setRawCapture(boolean picture, boolean video) {
            return this;
        }

        @Override
        public CommandManager setEnableCombinedYaw(boolean b) {
            return this;
        }

        @Override
        public CommandManager setFlyingMode(FlyingMode mode) {
            return this;
        }

        @Override
        public CommandManager setHoveringRange(int range) {
            return this;
        }

        @Override
        public CommandManager setMaxEulerAngle(float angle) {
            return this;
        }

        @Override
        public CommandManager setMaxEulerAngle(Location l, float angle) {
            return this;
        }

        @Override
        public CommandManager setMaxAltitude(int altitude) {
            return this;
        }

        @Override
        public CommandManager setMaxAltitude(Location l, int altitude) {
            return this;
        }

        @Override
        public CommandManager setMinAltitude(int altitude) {
            return this;
        }

        @Override
        public CommandManager setMinAltitude(Location l, int altitude) {
            return this;
        }

        @Override
        public CommandManager setMaxVz(int speed) {
            return this;
        }

        @Override
        public CommandManager setMaxVz(Location l, int speed) {
            return this;
        }

        @Override
        public CommandManager setMaxYaw(float speed) {
            return this;
        }

        @Override
        public CommandManager setMaxYaw(Location l, float speed) {
            return this;
        }

        @Override
        public CommandManager setCommand(ATCommand command) {
            return this;
        }

        @Override
        public CommandManager setOutdoor(boolean flying_outdoor, boolean outdoor_hull) {
            return this;
        }

        @Override
        public CommandManager animate(FlightAnimation a) {
            return this;
        }

        @Override
        public CommandManager setPosition(double latitude, double longitude, double altitude) {
            return this;
        }

        @Override
        public CommandManager setUltrasoundFrequency(UltrasoundFrequency f) {
            return this;
        }

        @Override
        public CommandManager setSSIDSinglePlayer(String ssid) {
            return this;
        }

        @Override
        public CommandManager setSSIDMultiPlayer(String ssid) {
            return this;
        }

        @Override
        public CommandManager setWifiMode(WifiMode mode) {
            return this;
        }

        @Override
        public CommandManager setOwnerMac(String mac) {
            return this;
        }

        @Override
        public CommandManager startRecordingNavData(String dirname) {
            return this;
        }

        @Override
        public CommandManager cancelRecordingNavData() {
            return this;
        }

        @Override
        public CommandManager stopRecordingNavData() {
            return this;
        }

        @Override
        public CommandManager startRecordingPictures(int delay, int nshots) {
            return this;
        }

        @Override
        public void run() {
            
        }

        @Override
        public CommandManager setControlAck(boolean b) {
            return this;
        }

        @Override
        public boolean connect(int port) {
            return true;
        }

        @Override
        public boolean isConnected() {
            return true;
        }

        @Override
        public void close() {
            
        }

        @Override
        public void stop() {
            fly = false;
        }

        @Override
        protected void ticklePort(int port) {
            
        }

        @Override
        public void start() {

            
        }
    }
    private AttitudeListener attitudeListener;

    public class DummyNavDataManager extends NavDataManager{



        public DummyNavDataManager(InetAddress inetaddr, CommandManager manager, IExceptionListener excListener) {
            super(null, new DummyCommandManager(null, null), new IExceptionListener() {
                @Override
                public void exeptionOccurred(ARDroneException exc) {
                    exc.printStackTrace();
                }
            });
        }

        @Override
        public void addAttitudeListener(AttitudeListener attitudeL) {

            attitudeListener = attitudeL;
        }

        @Override
        public void removeAttitudeListener(AttitudeListener attitudeListener) {
            
        }

        @Override
        public void addAltitudeListener(AltitudeListener altitudeListener) {
            
        }

        @Override
        public void removeAltitudeListener(AltitudeListener altitudeListener) {
            
        }

        @Override
        public void addBatteryListener(BatteryListener batteryListener) {
            
        }

        @Override
        public void removeBatteryListener(BatteryListener batteryListener) {
            
        }

        @Override
        public void addTimeListener(TimeListener timeListener) {
            
        }

        @Override
        public void removeTimeListener(TimeListener timeListener) {
            
        }

        @Override
        public void addStateListener(StateListener stateListener) {
            
        }

        @Override
        public void removeStateListener(StateListener stateListener) {
            
        }

        @Override
        public void addVelocityListener(VelocityListener velocityListener) {
            
        }

        @Override
        public void removeVelocityListener(VelocityListener velocityListener) {
            
        }

        @Override
        public void addVisionListener(VisionListener visionListener) {
            
        }

        @Override
        public void removeVisionListener(VisionListener visionListener) {
            
        }

        @Override
        public void addMagnetoListener(MagnetoListener magnetoListener) {
            
        }

        @Override
        public void removeMagnetoListener(MagnetoListener magnetoListener) {
            
        }

        @Override
        public void addAcceleroListener(AcceleroListener acceleroListener) {
            
        }

        @Override
        public void removeAcceleroListener(AcceleroListener acceleroListener) {
            
        }

        @Override
        public void addGyroListener(GyroListener gyroListener) {
            
        }

        @Override
        public void removeGyroListener(GyroListener gyroListener) {
            
        }

        @Override
        public void addUltrasoundListener(UltrasoundListener ultrasoundListener) {
            
        }

        @Override
        public void removeUltrasoundListener(UltrasoundListener ultrasoundListener) {
            
        }

        @Override
        public void addAdcListener(AdcListener adcListener) {
            
        }

        @Override
        public void removeAdcListener(AdcListener adcListener) {
            
        }

        @Override
        public void addCounterListener(CounterListener counterListener) {
            
        }

        @Override
        public void removeCounterListener(CounterListener counterListener) {
            
        }

        @Override
        public void addPressureListener(PressureListener pressureListener) {
            
        }

        @Override
        public void removePressureListener(PressureListener pressureListener) {
            
        }

        @Override
        public void addTemperatureListener(TemperatureListener temperatureListener) {
            
        }

        @Override
        public void removeTemperatureListener(TemperatureListener temperatureListener) {
            
        }

        @Override
        public void addWindListener(WindListener windListener) {
            
        }

        @Override
        public void removeWindListener(WindListener windListener) {
            
        }

        @Override
        public void addVideoListener(VideoListener videoListener) {
            
        }

        @Override
        public void removeVideoListener(VideoListener videoListener) {
            
        }

        @Override
        public void addWifiListener(WifiListener wifiListener) {
            
        }

        @Override
        public void removeWifiListener(WifiListener wifiListener) {
            
        }

        @Override
        public void addZimmu3000Listener(Zimmu3000Listener zimmu3000Listener) {
            
        }

        @Override
        public void removeZimmu3000Listener(Zimmu3000Listener zimmu3000Listener) {
            
        }

        @Override
        public void addPWMlistener(PWMlistener pwmlistener) {
            
        }

        @Override
        public void removePWMlistener(PWMlistener pwmlistener) {
            
        }

        @Override
        public void addReferencesListener(ReferencesListener referencesListener) {
            
        }

        @Override
        public void removeReferencesListener(ReferencesListener referencesListener) {
            
        }

        @Override
        public void addTrimsListener(TrimsListener trimsListener) {
            
        }

        @Override
        public void removeTrimsListener(TrimsListener trimsListener) {
            
        }

        @Override
        public void run() {
            
        }

        @Override
        public DroneState parse(ByteBuffer b) throws NavDataException {
            return new DroneState(0,0);
        }

        @Override
        public boolean connect(int port) {
            return true;
        }

        @Override
        public boolean isConnected() {
            return true;
        }

        @Override
        public void close() {
            
        }

        @Override
        public void stop() {
            
        }

        @Override
        protected void ticklePort(int port) {
            
        }

        @Override
        public void start() {
            
        }
    }
    @Override
    public CommandManager getCommandManager() {
        return new DummyCommandManager(null,null);
    }

    @Override
    public NavDataManager getNavDataManager() {
        return new DummyNavDataManager(null,null,null);
    }

    @Override
    public VideoManager getVideoManager() {
        return null;
    }

    @Override
    public ConfigurationManager getConfigurationManager() {
        return null;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void setHorizontalCamera() {

    }

    @Override
    public void setVerticalCamera() {

    }

    @Override
    public void setHorizontalCameraWithVertical() {

    }

    @Override
    public void setVerticalCameraWithHorizontal() {

    }

    @Override
    public void toggleCamera() {

    }

    @Override
    public void landing() {
        Log.i(TAG,"landing");
        fly = false;
    }

    @Override
    public void takeOff() {
        fly = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(fly){
                    float rand = (float) Math.random();
                    if(attitudeListener != null)
                        attitudeListener.attitudeUpdated(180000f*rand,180000f*rand,180000f*rand);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        Log.i(TAG,"Taking off");
    }

    @Override
    public void reset() {

    }

    @Override
    public void forward() {

    }

    @Override
    public void backward() {

    }

    @Override
    public void spinRight() {

    }

    @Override
    public void spinLeft() {

    }

    @Override
    public void up() {

    }

    @Override
    public void down() {

    }

    @Override
    public void goRight() {

    }

    @Override
    public void goLeft() {

    }

    @Override
    public void freeze() {

    }

    @Override
    public void hover() {

    }

    @Override
    public int getSpeed() {
        return 0;
    }

    @Override
    public void setSpeed(int speed) {

    }

    @Override
    public void addSpeedListener(ARDrone.ISpeedListener speedListener) {

    }

    @Override
    public void removeSpeedListener(ARDrone.ISpeedListener speedListener) {

    }

    @Override
    public void addExceptionListener(IExceptionListener exceptionListener) {

    }

    @Override
    public void removeExceptionListener(IExceptionListener exceptionListener) {

    }

    @Override
    public void setMaxAltitude(int altitude) {

    }

    @Override
    public void setMinAltitude(int altitude) {

    }

    @Override
    public void move3D(int speedX, int speedY, int speedZ, int speedSpin) {

    }
}
