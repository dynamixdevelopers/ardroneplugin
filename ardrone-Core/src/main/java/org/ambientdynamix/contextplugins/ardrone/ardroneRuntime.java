/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ardrone;

import java.util.*;

import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.RemoteException;
import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.exception.ARDroneException;
import de.yadrone.base.exception.IExceptionListener;
import de.yadrone.base.exception.NavDataException;
import de.yadrone.base.navdata.*;
import org.ambientdynamix.api.application.Callback;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.*;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.contextplugins.ambientcontrol.*;

/**
 * Example auto-reactive plug-in that detects the device's battery level.
 *
 * @author Darren Carlson
 */
public class ardroneRuntime extends ContextPluginRuntime {
    private static final int VALID_CONTEXT_DURATION = 60000;
    private static final int YAW_THRESHOLD = 15;
    private static final int ROLL_THRESHOLD = 10;
    private static final int PITCH_THRESHOLD = 10;
    // Static logging TAG
    private final String TAG = ((Object) this).getClass().getSimpleName();
    // Our secure context
    private Context context;

    private IARDrone drone = null;
    private boolean isFlying = false;
    private UUID requestId;
    private int controllerYawCalibration;
    private boolean isCalibrated = true;
    private int controllerRollCalibration;
    private int ControllerPitchCalibration;

    private final boolean USE_DUMMY = false;

    private ControlConnectionManager controlConnectionManager;

    private float currentPitch;
    private float currentYaw;
    private float currentRoll;

    private int desiredPitch;
    private int desiredYaw;
    private int desiredRoll;
    private float desiredAsscend;

    private float dronePitchCalibration;
    private float droneYawCalibration;
    private float droneRollCalibration;

    private double controllerGcallibration;
    private boolean isGcalibrated = true;

    private boolean droneConnected;
    private boolean controllerConnected;
    private boolean hover;
    private WifiManager wifi;
    private boolean running;

    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        this.setPowerScheme(powerScheme);
        // Store our secure context
        context = this.getSecuredContext();
//        TranslatingProfileMatcher translatingProfileMatcher = new TranslatingProfileMatcher();
//        translatingProfileMatcher.registerTranslator(org.ambientdynamix.contextplugins.ardrone.SingleValueToUpDownTranslator.class);

//        Map<String, ControlProfile> profiles = translatingProfileMatcher.getProfiles();
//        ControlProfile controlProfile = profiles.get("org.ambientdynamix.contextplugins.ardrone");
//        controlProfile.addOptional("PITCH DERIVATIVE");

//        ControlProfile pitchtrackerProfile = profiles.get("org.ambientdynamix.contextplugins.pitchtracker");
//        pitchtrackerProfile.addAvailableControl("PITCH DERIVATIVE","PITCH DERIVATIVE");

        controlConnectionManager = new ControlConnectionManager(this, clientListener, serverListener, new TranslatingProfileMatcher() , TAG);
        wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        if (!controlConnectionManager.addContextListener(listenerInfo))
            return super.addContextlistener(listenerInfo);
        else
            return true;
    }


    private Runnable watchdog = new Runnable() {
        @Override
        public void run() {
            while (isFlying) {
                if (!droneConnected || !controllerConnected) {
                    drone.landing();
                    isFlying = false;
                    if (!droneConnected) {
                        Log.e(TAG, "Watchdog lost drone, landing");
                        drone.getNavDataManager().stop();
                        drone.getNavDataManager().start();

                    } else if (!controllerConnected)
                        Log.e(TAG, "Watchdog lost controller, landing");
                } else {
                    droneConnected = false;
                    controllerConnected = false;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    };


    private long lastAccupdate;
    private ControlConnectionManager.IControllConectionManagerListener clientListener = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {

        @Override
        public void consumeCommand(IControlMessage command, String sourcePluginId, String request) {
            if (command instanceof ToggleCommand)
                consumeToggleCommand((ToggleCommand) command);
            else if (command instanceof PYRSensor) {
                consumePYRSensor((PYRSensor) command);
            }else if (command instanceof ACCSensor) {
                consumeAccSensor((ACCSensor) command);
            }else if (command instanceof ToggleSensor) {
                consumeToggleSensor((ToggleSensor) command);

            }
//            else if(command.getCommand().equals("PITCH DERIVATIVE")){
//                SingleValuePitch valuePitch = (SingleValuePitch) command;
//                Log.i(TAG, valuePitch.getCommand() + ": " + valuePitch.getValue());
//            }

        }

        @Override
        public Map<String, String> provideFeedbackSuggestion() {
            Log.i(TAG, "suggesting feedback:" + Commands.DISPLAY_COLOR.toString());
            HashMap<String, String> commandsEnums = new HashMap<String, String>();
            commandsEnums.put("Orientation Color", Commands.DISPLAY_COLOR);
            return commandsEnums;
        }

        @Override
        public void controllRequest(String command, String name) {
            if (!sendDisplayFeedback && command.equals(Commands.DISPLAY_COLOR)) ;
            {
                Log.i(TAG, "starting feedback:" + Commands.DISPLAY_COLOR.toString());
                sendDisplayFeedback = true;
            }
        }

        @Override
        public void stopControlling(String command, String name) {
            if (drone != null) {
                drone.landing();
                isFlying = false;
            }
            if (command.equals(Commands.DISPLAY_COLOR)) {
                sendDisplayFeedback = false;
            }
        }

        public void consumeToggleCommand(ToggleCommand ToggleCommand) {
            Log.i(TAG,"consuming " + ToggleCommand.getCommand());
            controllerConnected = true;
            String command = ToggleCommand.getCommand();
            float speed = (float) (ToggleCommand.getVelocity() / 100.0f);
            if (command.equals(Commands.MOVEMENT_START_STOP)) {
                startOrLand();
                return;
            } else if (command.equals(Commands.MOVEMENT_BACKWARD)) {
                desiredRoll = 0;
                desiredPitch = 45;
                return;
            } else if (command.equals(Commands.MOVEMENT_FORWARD)) {
                desiredRoll = 0;
                desiredPitch = -45;
                return;
            } else if (command.equals(Commands.MOVEMENT_LEFT)) {
                desiredPitch = 0;
                desiredRoll = -45;
                return;
            } else if (command.equals(Commands.MOVEMENT_RIGHT)) {
                desiredPitch = 0;
                desiredRoll = 45;
                return;
            } else if (command.equals(Commands.MOVEMENT_UP)) {
                desiredAsscend = (float) ToggleCommand.getVelocity();
//                    Log.i(TAG,"MOVE UP");
                lastAccupdate = System.currentTimeMillis();
                return;
            } else if (command.equals(Commands.MOVEMENT_DOWN)) {
//                    Log.i(TAG,"MOVE DOWN");
                lastAccupdate = System.currentTimeMillis();
                desiredAsscend = (float) ToggleCommand.getVelocity();
                return;
            } else if (command.equals(Commands.MOVEMENT_NEUTRAL)) {
                desiredRoll = 0;
                desiredPitch = 0;
                return;
            }
        }


        public void consumePYRSensor(PYRSensor pyrSensor) {
            controllerConnected = true;
//            Log.i(TAG,"consuming " + pyrSensor.getContextType());
            if (!isCalibrated)
                calibrate(pyrSensor);
            if (isFlying) {
                desiredPitch = pyrSensor.getPitch();
                desiredRoll = pyrSensor.getRoll();
                desiredYaw = pyrSensor.getYaw();
//                if(roll2float(control.getRoll()) != 0.0f || pitch2float(control.getPitch())!= 0.0f ){
//                    lastCommand = System.currentTimeMillis();
//                }else if(System.currentTimeMillis() > 1000){
//                    isCalibrated = false;
//                    lastCommand = System.currentTimeMillis();
//                }


            }
        }

        public void consumeAccSensor(ACCSensor accSensor) {
            controllerConnected = true;
//            Log.i(TAG,"ACC RAW (x,y,z): " + accSensor.getX() + ", " + accSensor.getY() + ", " + accSensor.getZ() );
            if (!isGcalibrated) {
                controllerGcallibration = accSensor.getZ();
                isGcalibrated = true;
            }
            if (Math.abs(accSensor.getZ() - 0.981) > 0.1) {
                if (Math.signum(accSensor.getZ() - 0.981) > 0) {
//                    Log.i(TAG,"MOVE UP " + Math.abs(accSensor.getZ() - 0.981));
                    desiredAsscend = 0.5f;
                } else {
//                     Log.i(TAG,"MOVE DOWN " + Math.abs(accSensor.getZ() - 0.981));
                    desiredAsscend = -0.5f;
                }
                lastAccupdate = System.currentTimeMillis();
            } else
                desiredAsscend = 0.0f;
        }


        public void consumeToggleSensor(ToggleSensor ToggleSensor) {
            controllerConnected = true;
            Log.i(TAG, "consuming " + ToggleSensor.getContextType());
            startOrLand();
        }

    };

    private ControlConnectionManager.IControllConectionManagerListener serverListener = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {

        @Override
        public void controllRequest(String command, String name) {
            Log.i(TAG, "init control logic for " + command);
            if (command.equals(Commands.SENSOR_PYR)) {
                drone.getNavDataManager().addAttitudeListener(attitudeListener);
            } else if (command.equals(Commands.SENSOR_GYRO)) {
                drone.getNavDataManager().addGyroListener(gyroListener);
            } else if (command.equals(Commands.SENSOR_ACC)) {
                drone.getNavDataManager().addAcceleroListener(acceleroListener);
            }
        }

        @Override
        public void stopControlling(String command, String name) {
            Log.i(TAG, "stopping controlling " + command);
            if (drone == null)
                return;
            if (command.equals(Commands.SENSOR_PYR)) {
                drone.getNavDataManager().removeAttitudeListener(attitudeListener);
            } else if (command.equals(Commands.SENSOR_GYRO)) {
                drone.getNavDataManager().removeGyroListener(gyroListener);
            } else if (command.equals(Commands.SENSOR_ACC)) {
                drone.getNavDataManager().removeAcceleroListener(acceleroListener);
            }
        }
    };

    @Override
    public void onMessageReceived(Message message) {
        try {
            controlConnectionManager.handleConfigCommand(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private Runnable navigator = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "Navigator started");
            while (isFlying && drone != null) {
                float yawOffset = getHeadingDifference(getCalibratedHeading(desiredYaw, controllerYawCalibration), getCalibratedHeading((int) currentYaw, droneYawCalibration));
                if (Math.abs(yawOffset) < YAW_THRESHOLD)
                    yawOffset = 0f;
                Log.i(TAG, "roll: " + roll2float(desiredRoll) + " pitch: " + pitch2float(desiredPitch) + " spin: " + yawOffset / -180f + " ch: (" + currentYaw + ") " + getCalibratedHeading((int) currentYaw, droneYawCalibration) + " dh: (" + desiredYaw + ") " + getCalibratedHeading(desiredYaw, controllerYawCalibration)
                        + "calib: " + controllerYawCalibration);
                if (System.currentTimeMillis() - lastAccupdate > 1000)
                    desiredAsscend = 0f;
                if (!hover && drone != null)
                    drone.getCommandManager().move(roll2float(desiredRoll), pitch2float(desiredPitch), desiredAsscend, yawOffset / -180f);
                else
                    drone.getCommandManager().move(0, 0, 0, 0);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.i(TAG, "Navigator stopped");
        }
    };

    float dampening = 0.8f;

    private float getSpin(int yaw) {
        float calibratedYaw = 0.0f;

        if (controllerYawCalibration > 0) {
            if (yaw - controllerYawCalibration < -180) {
                calibratedYaw = 360.0f + yaw - controllerYawCalibration;
            } else
                calibratedYaw = yaw - controllerYawCalibration;
        } else {
            if (yaw - controllerYawCalibration > 180) {
                calibratedYaw = -360.0f + yaw - controllerYawCalibration;
            } else
                calibratedYaw = yaw - controllerYawCalibration;
        }
        if (Math.abs(calibratedYaw) > YAW_THRESHOLD)
            return (calibratedYaw - Math.signum(calibratedYaw) * YAW_THRESHOLD) / -90.0f;
        else
            return 0.0f;
    }

    private float getCalibratedHeading(int yaw, float callibration) {
        float calibratedYaw = 0.0f;

        if (callibration > 0) {
            if (yaw - callibration < -180) {
                calibratedYaw = 360.0f + yaw - callibration;
            } else
                calibratedYaw = yaw - callibration;
        } else {
            if (yaw - callibration > 180) {
                calibratedYaw = -360.0f + yaw - callibration;
            } else
                calibratedYaw = yaw - callibration;
        }
        return calibratedYaw;
    }

    private float getHeadingDifference(float desiredHeading, float currentHeading) {
        float headingDifference;
        if (desiredHeading >= 0f && currentHeading >= 0f)
            headingDifference = desiredHeading - currentHeading;
        else if (desiredHeading < 0f && currentHeading < 0f)
            headingDifference = desiredHeading - currentHeading;
        else if (currentHeading > desiredHeading)
            if (desiredHeading - currentHeading > -180f)
                headingDifference = desiredHeading - currentHeading;
            else
                headingDifference = 360f + desiredHeading - currentHeading;
        else if (desiredHeading - currentHeading < 180f)
            headingDifference = desiredHeading - currentHeading;
        else
            headingDifference = -360f + desiredHeading - currentHeading;

        Log.i(TAG, "Heading difference: " + headingDifference);
        return headingDifference;
    }

    private float pitch2float(int pitch) {
        if (Math.abs(pitch - ControllerPitchCalibration) < PITCH_THRESHOLD)
            return 0.0f;
//        return dampening * (pitch - Math.signum(pitch) * PITCH_THRESHOLD) / 90.0f;
        return dampening * (pitch - Math.signum(pitch) * PITCH_THRESHOLD - ControllerPitchCalibration) / 90.0f;
    }

    private float roll2float(int roll) {
        if (Math.abs(roll - controllerRollCalibration) < ROLL_THRESHOLD)
            return 0.0f;
//        return dampening * (roll - Math.signum(roll) * ROLL_THRESHOLD) / 90.0f;
        return dampening * (roll - Math.signum(roll) * ROLL_THRESHOLD - controllerRollCalibration) / 90.0f;
    }

    IExceptionListener exceptionListener = new IExceptionListener() {
        @Override
        public void exeptionOccurred(ARDroneException exc) {
            if (exc instanceof NavDataException) {
                Log.i(TAG, "Lost NavData connection trying reconnect");
                stopDrone();
                connect(USE_DUMMY);
            } else {
                Log.w(TAG, "exception occured: " + exc);
            }
        }
    };

    private void connect(final boolean dummy) {
        if (drone != null)
            return;
        if (dummy) {
            Log.i(TAG, "Connected to using dummy drone");
            drone = new DummyDrone();
            return;
        }

        if (!wifi.getConnectionInfo().getSSID().equals("\"ardrone2_052616\"")) {
            Log.i(TAG, "connected to: " + wifi.getConnectionInfo().getSSID() + " waiting for drone to connect ");
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        Thread.sleep(1000);
                        if (getPluginFacade().getState(getSessionId()).equals(PluginState.STARTED))
                            connect(dummy);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();
            return;
        }

        Log.i(TAG, "Connected to " + wifi.getConnectionInfo().getSSID());
        drone = new ARDrone("192.168.1.1", null); // null because of missing video support on Android

        try {
            drone.addExceptionListener(exceptionListener);
            drone.start();
            drone.reset();

            drone.getNavDataManager().addAttitudeListener(flightDataAttitudeListener);
        } catch (Exception exc) {
            exc.printStackTrace();

            if (drone != null)
                drone.stop();
        }
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {
        try {
            getPluginFacade().getDynamixFacade(getSessionId()).openSession();
            running = true;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Started!");
        connect(USE_DUMMY);
    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {
        Log.i(TAG, "Stopped!");
        running = false;
        controlConnectionManager.stop(new Callback());
        stopDrone();
    }

    private void stopDrone() {
        if (drone != null) {
            drone.landing();
            isFlying = false;
            drone.getNavDataManager().removeAttitudeListener(attitudeListener);
            drone.getNavDataManager().removeAcceleroListener(acceleroListener);
            drone.getNavDataManager().removeGyroListener(gyroListener);
            drone.removeExceptionListener(exceptionListener);
            drone.stop();
        }
        drone = null;
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        stopDrone();
        controlConnectionManager.stop(new Callback());
        context = null;
        Log.i(TAG, "Destroyed!");
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {

//        Log.i(TAG, "Received context request");
        // Check for proper context type
        if (contextType.equalsIgnoreCase(ArdroneContext.CONTEXT_TYPE)) {
            this.requestId = requestId;
        } else {
            sendContextRequestError(requestId, "NO_CONTEXT_SUPPORT for " + contextType, ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
        }
    }

    private void startOrLand() {

        if (!isFlying) {
            Log.i(TAG, "Trying to startOrLand");
            drone.reset();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    new Thread(watchdog).start();
                    drone.takeOff();
                    hover = true;
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    isCalibrated = false;
                    isDroneCalibrated = false;
                    isGcalibrated = false;
                    hover = false;
                    isFlying = true;
                    new Thread(navigator).start();
                }
            }).start();
        } else {
            isFlying = !isFlying;
            drone.landing();
        }

    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
//        Log.i(TAG, "Received configured context request");
        if (contextType.equalsIgnoreCase(ArdroneContext.CONTEXT_TYPE) && config.getBoolean("takeoff")) {
            startOrLand();
        } else
            controlConnectionManager.handleConfiguredContextRequest(requestId, contextType, config);
    }


    public void calibrate(PYRSensor control) {
        if (isCalibrated == false) {
            Log.i(TAG, "Calibrating pyr: " + control.getPitch() + " " + control.getYaw() + " " + control.getRoll());
            controllerYawCalibration = control.getYaw();
            controllerRollCalibration = control.getRoll();
            ControllerPitchCalibration = control.getPitch();

            isCalibrated = true;
        }
//        TODO: idea Recalibrate if accelerometer data shows steady hand for a while
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }

    private AttitudeListener attitudeListener = new AttitudeListener() {
        private int frequency = 200;
        private long lastUpdate = System.currentTimeMillis();

        @Override
        public void attitudeUpdated(float pitch, float roll, float yaw) {
            if (System.currentTimeMillis() - lastUpdate < frequency)
                return;
            lastUpdate = System.currentTimeMillis();
            pitch = pitch / 1000;
            roll = roll / 1000;
            yaw = yaw / 1000;
//            Log.i(TAG,"attitude update PYR: " + pitch + " " + yaw + " " + roll);
            controlConnectionManager.sendControllCommand(new PYRSensor((int) pitch, (int) yaw, (int) roll, "Pitch Yaw Roll"),wifi.getConnectionInfo().getSSID());
        }

        @Override
        public void attitudeUpdated(float pitch, float roll) {

        }

        @Override
        public void windCompensation(float pitch, float roll) {

        }
    };

    private boolean isDroneCalibrated;
    private boolean sendDisplayFeedback;
    long lastFeedback = -1;
    private AttitudeListener flightDataAttitudeListener = new AttitudeListener() {
        @Override
        public void attitudeUpdated(float pitch, float roll, float yaw) {
            droneConnected = true;
//            Log.i(TAG,"flightDataUpdate: " + yaw);
            if (!isDroneCalibrated) {
                droneYawCalibration = yaw / -1000f;
                isDroneCalibrated = true;
            }
            currentPitch = pitch / 1000;
            currentYaw = yaw / -1000f;
            currentRoll = roll / 1000;

            if (sendDisplayFeedback && (System.currentTimeMillis() - lastFeedback) > 300) {
//                Log.i(TAG,"sending color feedback");
                controlConnectionManager.sendControllCommand(getHeadingColor((int) currentRoll, (int) currentPitch),wifi.getConnectionInfo().getSSID());
                lastFeedback = System.currentTimeMillis();
            }
        }

        private DisplayCommand getHeadingColor(int roll, int pitch) {
            DisplayCommand command;
            if (Math.abs(roll) > ROLL_THRESHOLD || Math.abs(pitch) > PITCH_THRESHOLD) {
                float pitchNorm = (float) pitch / (float) Math.max(Math.abs(pitch), Math.abs(roll));
                float rollNorm = (float) roll / (float) Math.max(Math.abs(pitch), Math.abs(roll));
                final float heading;
                final float speed;
//                Log.i(TAG,"calculating display feedback");

                if (pitchNorm < 0) { //forward
                    if (rollNorm < 0) { //left 270 - 359
                        heading = (315f - pitchNorm * 45f + rollNorm * 45f);
                        command = new DisplayCommand(Commands.DISPLAY_COLOR, (int) (255 + 255 * (270 - heading) / 90.0), 255, 0, "Orientation Color");
                    } else { // right 0 - 90
                        heading = (45f + pitchNorm * 45f + rollNorm * 45f);
                        command = new DisplayCommand(Commands.DISPLAY_COLOR, 0, (int) (255 + 255 * (0 - heading) / 90.0), (int) (0 - 255 * (0 - heading) / 90.0), "Orientation Color");
                    }
                } else { //backward
                    if (rollNorm < 0) { //left 180 - 270
                        heading = (225f - pitchNorm * 45f - rollNorm * 45f);
                        command = new DisplayCommand(Commands.DISPLAY_COLOR, 255, (int) (255 - 255 * (270 - heading) / 90.0), 0, "Orientation Color");
                    } else { // right 90 - 180
                        heading = (135 + pitchNorm * 45f - rollNorm * 45f);
                        command = new DisplayCommand(Commands.DISPLAY_COLOR, (int) (255 - 255 * (180 - heading) / 90.0), 0, (int) (255 + 255 * (90 - heading) / 90.0), "Orientation Color");
                    }
                }
                speed = 2 * (Math.abs(roll) + Math.abs(pitch)) / 360f;
            } else
                command = new DisplayCommand(Commands.DISPLAY_COLOR, 0, 0, 0, "Orientation Color");
            return command;
        }

        @Override
        public void attitudeUpdated(float pitch, float roll) {

        }

        @Override
        public void windCompensation(float pitch, float roll) {

        }
    };

    private GyroListener gyroListener = new GyroListener() {

        @Override
        public void receivedRawData(GyroRawData d) {
            controlConnectionManager.sendControllCommand(new GyroSensor((int) d.getRawGyros()[0], (int) d.getRawGyros()[1], (int) d.getRawGyros()[2], "Gyroscope"),wifi.getConnectionInfo().getSSID());
        }

        @Override
        public void receivedPhysData(GyroPhysData d) {

        }

        @Override
        public void receivedOffsets(float[] offset_g) {

        }
    };

    private AcceleroListener acceleroListener = new AcceleroListener() {
        @Override
        public void receivedRawData(AcceleroRawData d) {
            controlConnectionManager.sendControllCommand(new ACCSensor((int) d.getRawAccs()[0], (int) d.getRawAccs()[1], (int) d.getRawAccs()[2], "Accelerometer"),wifi.getConnectionInfo().getSSID());
        }

        @Override
        public void receivedPhysData(AcceleroPhysData d) {
        }
    };
}