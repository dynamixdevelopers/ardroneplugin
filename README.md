# The ArDrone Plugin
This plugin allows to control the Parrot ArDrone version 2 and receive sensor data from its sensors.

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:

```JSON
{
    "name":"ardrone",
    "artifact_id":"org.ambientdynamix.contextplugins.ardrone",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[
        {
            "mandatoryControls":[
                "SENSOR_PYR",
                "MOVEMENT_START_STOP"
            ],
            "priority":1
        },
        {
            "mandatoryControls":[
                "SENSOR_AXIS"
            ],
            "priority":2
        },
        {
            "mandatoryControls":[
                "MOVEMENT_FORWARD_LEFT",
                "MOVEMENT_BACKWARD",
                "MOVEMENT_BACKWARD_LEFT",
                "MOVEMENT_LEFT",
                "MOVEMENT_RIGHT",
                "MOVEMENT_START_STOP",
                "MOVEMENT_FORWARD",
                "MOVEMENT_FORWARD_RIGHT",
                "MOVEMENT_BACKWARD_RIGHT"
            ],"priority":3
        }
    ],
    "outputList":{
        "Accelerometer":"SENSOR_ACC",
        "Pitch Yaw Roll":"SENSOR_PYR",
        "Gyroscope":"SENSOR_GYRO"
    },
    "optionalInputList":[
        "MOVEMENT_DOWN",
        "MOVEMENT_UP"
    ]
}
```